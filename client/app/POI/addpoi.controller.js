        // Always use an IIFE, i.e., (function() {})();
        (function() {
            angular
                .module("TravelGuruApp")          // to call an angular module, omit the second argument ([]) from the angular.module() syntax
                // this syntax is called the getter syntax
                .controller("AddPoiCtrl", AddPoiCtrl) // angular.controller() attaches a controller to the angular module specified
                

            
            AddPoiCtrl.$inject = [ '$state','$http' ];

            function AddPoiCtrl( $state, $http) {
                var addpoiCtrl = this;

                addpoiCtrl.newPoi = {};
                //     addpoiCtrl.city="",
                //     addpoiCtrl.poiname ="",
                //     addpoiCtrl.description=[],
                //     addpoiCtrl.category ="",
                //     addpoiCtrl.createby=""
                // };

                // addpoiCtrl.newPlace = function () {

                // }
                // addpoiCtrl.newPoi = newPoi;

                addpoiCtrl.addNewPoi = function () {
                    console.log("value of addpoiCtrl.newPoi: ", addpoiCtrl.newPoi);
                    $http.post('/api/pois', addpoiCtrl.newPoi)
                        .then(function(result) {
                            $state.go('discover');
                            console.log ("New POI entry added: ", result);
                        }).catch (function(e) {
                            console.log(e);
                        });
                    
                };

            };

            
            
        })();
    // Always use an IIFE, i.e., (function() {})();
    (function() {
        angular
            .module("TravelGuruApp")          // to call an angular module, omit the second argument ([]) from the angular.module() syntax
            // this syntax is called the getter syntax
            .controller("PoiCtrl", PoiCtrl) // angular.controller() attaches a controller to the angular module specified
            

        
        PoiCtrl.$inject = [ '$state', '$http' ];

        function PoiCtrl( $state, $http) {
            var poiCtrl = this;


            // poiCtrl.allpois = "";

            // var getPois = function () {
            //     poiCtrl.findPois ()
            //     .then (function (results){

            //     })

            // }
             poiCtrl.allPois = {}
             poiCtrl.keyword = "";
             var keyword = poiCtrl.keyword;


            var getAllPois = function () {
                $http.get('/api/pois', {keyword: poiCtrl.keyword})
                    .then (function (results) {
                        console.log('Content of results: ', results);
                        poiCtrl.allPois = results.data;
                    }).catch (function (err){
                        console.error ("Error encountered : ", err);
                    })
                
            }
            getAllPois();

            poiCtrl.findAll = function (keyword) {
                console.log(keyword);
                $http.get('/pois', keyword)
                .then(function(keyword) {
                    $state.go('discover');
                    console.log(keyword);
                }).catch(function(e) {
                    console.log(e);
                });
            }

            poiCtrl.viewPoi = function(poiIdToView) {
                console.log('value of incoming POI to view: ', poiIdToView);
                $state.go('review', {poiId: poiIdToView});
            };



        };

        
    })();
// Defines client-side routing
(function () {
    angular
        .module("TravelGuruApp")
        .config(travelGuruConfig)
    travelGuruConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function travelGuruConfig($stateProvider, $urlRouterProvider) {

        $stateProvider   

            .state('index', {
            url: '/index',
            // templateUrl: 'index.html',
            template: '<ui-view></ui-view>',
            
            // data: 
            //     {
            //     css: ['assets/css/index.css']
            //   }
            })

            .state('splash', {
                url: '/splash',
                templateUrl: 'app/splash/splash.html',
                // data: 
                //     {
                //     css: ['assets/css/styles.css']
                //   }
            })
            .state('logIn', {
                url: '/logIn',
                templateUrl: 'app/registration/login.html',
                controller: 'LoginCtrl',
                controllerAs: 'loginCtrl',
                // css: 'client/assets/css/login.css'
                        
            })
            .state('discover', {
                url: '/discover',
                templateUrl: 'app/poi/poi.html',
                controller: 'PoiCtrl',
                controllerAs:'poiCtrl'
            })
            .state('review', {
                url: '/review/:poiId',
                templateUrl: 'app/review/review.html',
                controller: 'ReviewCtrl',
                controllerAs:'reviewCtrl'
                // details on specific review
            })
            .state('addpoi', {
                url: '/addpoi',
                templateUrl: 'app/poi/addpoi.html',
                controller: 'AddPoiCtrl',
                controllerAs:'addpoiCtrl'
            
            })

            .state('addreview', {
                url: '/addreview',
                templateUrl: 'app/review/addreview.html',
                controller: 'AddReviewCtrl',    
                controllerAs:'addReviewCtrl'
            
            })
            
            .state('register', {
                url: '/register',
                templateUrl: 'app/registration/register.html',
                controller: 'RegisterCtrl',
                controllerAs:'registerCtrl'
            });

        $urlRouterProvider.otherwise('/splash');
    }
})();
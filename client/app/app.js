// Always use an IIFE, i.e., (function() {})();
(function() {
    'use strict'
    // Creates a new module
    // When setting (creating) an angular module, you need to specify the second argument ([ ])
    // Without this argument, we are telling Angular that what we want to do is to get an already existing module
    angular.module('TravelGuruApp', [ 'ui.router', 'uiRouterStyles' ]);// ui-router is a client-side Single Page Application routing framework for AngularJS
            
})();

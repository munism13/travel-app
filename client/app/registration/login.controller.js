(function() {
    angular
        .module("TravelGuruApp")         
        
        .controller("LoginCtrl", LoginCtrl);    
                                        
    LoginCtrl.$inject = [ '$state','$http' ];

    function LoginCtrl($state,$http) {
        var loginCtrl = this;

        loginCtrl.email= "";
        loginCtrl.password="";

        loginCtrl.rememberMe = function () {
            console.log ("Remember: " + loginCtrl.user.username + loginCtrl.user.password);

        }
        
         loginCtrl.logInUser = function() {
            console.log(loginCtrl.email);
            console.log(loginCtrl.password);
            $http.post('/login', {"email": loginCtrl.email, "password": loginCtrl.password})
                .then(function(result) {
                    $state.go('discover');
                    console.log(result);
                }).catch(function(e) {
                    console.log(e);
                });
        };

           

        // } // END function register()
    } // END RegCtrl
})();

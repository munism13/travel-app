
(function () {
    angular
        .module("TravelGuruApp")
        .controller("RegisterCtrl", RegisterCtrl);

        RegisterCtrl.$inject = [ '$state','$http' ];

    function RegisterCtrl($state, $http){
        var registerCtrl = this;

        // registerCtrl.username = "";
        // registerCtrl.email = "";
        // registerCtrl.password = "";
        // registerCtrl.firstName = "";
        // registerCtrl.lastName = "";
        // registerCtrl.dob = ""
        registerCtrl.user ={};

        // console.log ("Username: ",  registerCtrl.username );
        // console.log ("Email: " , registerCtrl.email );
        // console.log ("Password" , registerCtrl.password );
        // console.log ("First Name: " , registerCtrl.firstName );
        // console.log ("Last Name: " , registerCtrl.lastName);
        // console.log ("DOB: " , registerCtrl.dob )

        registerCtrl.rememberMe = function () {
            // console.log ("Remember: " + registerCtrl.user.username + loginCtrl.user.password);

        }


        registerCtrl.registerUser = function() {
            console.log('value of registerCtrl.user:', registerCtrl.user);
            $http.post('/api/users', registerCtrl.user)
                .then(function(result) {
                    $state.go('discover');
                    console.log(result);
                }).catch(function(e) {
                    console.log(e);
                });
        }

    

    }
})();
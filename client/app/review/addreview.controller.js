        // Always use an IIFE, i.e., (function() {})();
        (function() {
            angular
                .module("TravelGuruApp")          // to call an angular module, omit the second argument ([]) from the angular.module() syntax
                // this syntax is called the getter syntax
                .controller("AddReviewCtrl", AddReviewCtrl) // angular.controller() attaches a controller to the angular module specified
                

            
            AddReviewCtrl.$inject = [ '$state','$http' ];

            function AddReviewCtrl( $state, $http) {
                var addReviewCtrl = this;


                addReviewCtrl.newReview = {};
            //     (
            //     addReviewCtrl.comments="",
            //     addReviewCtrl.rating ="",
            //     addReviewCtrl.poi_id=[],
            //     addReviewCtrl.user_id =""
                
            // )};

                addReviewCtrl.addNewReview = function () {
                    console.log("value of addReviewCtrl.newReview: ", addReviewCtrl.newReview);
                    // $http.post('/api/reviews/', { review: addNewReview})
                    $http.post('/api/reviews/', addReviewCtrl.newReview)
                        .then(function() {
                            $state.go('discover');
                            console.log ("New review entry added: ", addNewReview);
                        }).catch (function(e) {
                            console.log(e);
                        });
                    
                };

            };
            
        })();
        // Always use an IIFE, i.e., (function() {})();
        (function() {
            angular
                .module("TravelGuruApp")          // to call an angular module, omit the second argument ([]) from the angular.module() syntax
                // this syntax is called the getter syntax
                .controller("ReviewCtrl", ReviewCtrl) // angular.controller() attaches a controller to the angular module specified
                

            
            ReviewCtrl.$inject = [ '$state', '$stateParams', '$http' ];

            function ReviewCtrl( $state, $stateParams, $http) {
                var reviewCtrl = this;
                thisPoi = parseInt($stateParams.poiId);

                reviewCtrl.allReviewsForThisPoi = {};

                var getAllReviewsForThisPoi = function (poiId) {
                    console.log("...", poiId);
                    $http.get('/api/pois/' + poiId)
                        .then(function(results){
                            reviewCtrl.allReviewsForThisPoi = results.data;
                            console.log(results);
                        }).catch(function(e) {
                            console.log(e);
                    });
                };
                // populate initial set of reviews for POI
                getAllReviewsForThisPoi(thisPoi);

                reviewCtrl.getAllReviewsForThisPoi = getAllReviewsForThisPoi;

                reviewCtrl.retrievePoiDetails = function (poiId) {
                    console.log ("Ready to display this POI info.. ", poiId);
                    $http.get('api/poi/' + poiId)
                        .then(function(results){
                            console.log(results);
                        }).catch (function (e){
                            console.log(e);
                        })
                }

            };      
        })();
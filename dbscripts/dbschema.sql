CREATE DATABASE if not exists `travelkaki` ;
USE `travelkaki`;

-- table to store user data
CREATE TABLE if not exists `user`
(
  `id` INT(11) AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(64) NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `nickname` VARCHAR(45),
  `nationality` VARCHAR(45),
  `createdAt` DATETIME,
  `updatedAt` DATETIME,
  `deletedAt` DATETIME,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC)
);

-- table to store POI data
CREATE TABLE if not exists `poi`
(
  `id` INT(11) AUTO_INCREMENT,
  `city` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45),
  `poiname` VARCHAR(45) NOT NULL,
  `description` VARCHAR(255),
  `category` varchar(30) NOT NULL,
  `imagelink` VARCHAR(255),
  `createby` int(11) NOT NULL,
  `createdAt` DATETIME,
  `updatedAt` DATETIME,
  `deletedAt` DATETIME,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`createby`) REFERENCES `travelkaki`.`user`(`id`)
);

-- table to store reviews of POI
CREATE TABLE if not exists `review`
(
  `id` INT(11) AUTO_INCREMENT,
  `comments` VARCHAR(255) NOT NULL,
  `rating` INT(1) NOT NULL,
  `poi_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `createdAt` DATETIME,
  `updatedAt` DATETIME,
  `deletedAt` DATETIME,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`poi_id`) REFERENCES `travelkaki`.`poi`(`id`),
  FOREIGN KEY (`user_id`) REFERENCES `travelkaki`.`user`(`id`)
);

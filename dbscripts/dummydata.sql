-- insert 10 dummy users
INSERT INTO `travelkaki`.`user` (`email`, `password`, `firstname`, `lastname`, `nickname`) VALUES ('honmun@gmail.com', '1234', 'Lim', 'Choon Beng', 'CB Lim');
INSERT INTO `travelkaki`.`user` (`email`, `password`, `firstname`, `lastname`, `nickname`) VALUES ('h.onmun@gmail.com', '1234', 'Chris', 'Ho', 'XHO');
INSERT INTO `travelkaki`.`user` (`email`, `password`, `firstname`, `lastname`, `nickname`) VALUES ('ho.nmun@gmail.com', '1234', 'Mohd', 'Izhan', 'izhan');
INSERT INTO `travelkaki`.`user` (`email`, `password`, `firstname`, `lastname`, `nickname`) VALUES ('hon.mun@gmail.com', '1234', 'Jeffrey', 'K', 'Captain');
INSERT INTO `travelkaki`.`user` (`email`, `password`, `firstname`, `lastname`, `nickname`) VALUES ('honm.un@gmail.com', '1234', 'Hong Lim', 'Teh', 'teh65');
INSERT INTO `travelkaki`.`user` (`email`, `password`, `firstname`, `lastname`, `nickname`) VALUES ('honmu.n@gmail.com', '1234', 'Saravanan', 'Palavathy', 'John');
INSERT INTO `travelkaki`.`user` (`email`, `password`, `firstname`, `lastname`, `nickname`) VALUES ('daniel@gmail.com', '1234', 'Caroline', 'Boo', 'Carol');
INSERT INTO `travelkaki`.`user` (`email`, `password`, `firstname`, `lastname`, `nickname`) VALUES ('d.aniel@gmail.com', '1234', 'Billie', 'Zhang', 'bilylovestennis');
INSERT INTO `travelkaki`.`user` (`email`, `password`, `firstname`, `lastname`, `nickname`) VALUES ('da.niel@gmail.com', '1234', 'Karolina', 'Voon', 'karolina');
INSERT INTO `travelkaki`.`user` (`email`, `password`, `firstname`, `lastname`, `nickname`) VALUES ('dan.iel@gmail.com', '1234', 'William', 'Png', 'William');

-- insert 10 dummy POIs
INSERT INTO `travelkaki`.`poi` (`city`, `poiname`, `description`, `category`, `createby`) VALUES ('Bali', 'Tanah Lot', 'An ancient Hindu shrine perched on top of an outcrop. one of Bali’s most important landmarks, famed for its unique offshore setting and sunset backdrops.', 'Tourist Attraction', 1);
INSERT INTO `travelkaki`.`poi` (`city`, `poiname`, `description`, `category`, `createby`) VALUES ('Bali', 'Splash Water Park', 'Splash Water Park features exciting water slides and activities that take fun to a whole new level!', 'Recreation', 1);
INSERT INTO `travelkaki`.`poi` (`city`, `poiname`, `description`, `category`, `createby`) VALUES ('Bali', 'Hard Rock Cafe', 'Visit Hard Rock Cafe Bali for exciting live entertainment. The cafe is marked by an iconic double-neck giant guitar, a replica of an original guitar from well-known guitarist Balawan', 'Entertainment', 1);
INSERT INTO `travelkaki`.`poi` (`city`, `poiname`, `description`, `category`, `createby`) VALUES ('Bali', 'Five Art Studio (Ubud)', 'Offers Balinese art classes and courses, including Keliki painting, to walk-in guests and tour groups', 'Entertainment', 1);
INSERT INTO `travelkaki`.`poi` (`city`, `poiname`, `description`, `category`, `createby`) VALUES ('Bali', 'Merah Putih Bali', 'Indonesian classics are served along side creative dishes that experiment with traditional spices & flavours from across the archipelago.', 'Food & Drink', 1);
INSERT INTO `travelkaki`.`poi` (`city`, `poiname`, `description`, `category`, `createby`) VALUES ('Bangkok', 'Grand Palace', 'Built in 1782. Beautiful architecture and intricate detail. Home of the Thai King, the Royal court and central administrative seat of government', 'Tourist Attraction', 3);
INSERT INTO `travelkaki`.`poi` (`city`, `poiname`, `description`, `category`, `createby`) VALUES ('Bangkok', 'Dream Bangkok', 'Hotel in central location, bathed in a glow of neons – blues, greens, oranges, pinks for a surrealistic avant-garde exterior', 'Lodging', 3);
INSERT INTO `travelkaki`.`poi` (`city`, `poiname`, `description`, `category`, `createby`) VALUES ('Bangkok', 'Blu-O Rhythm & Bowl', 'One of the older bowling alleys in Bangkok. On the top floor of central Major Ratchayothin mall', 'Recreation', 3);
INSERT INTO `travelkaki`.`poi` (`city`, `poiname`, `description`, `category`, `createby`) VALUES ('Bangkok', 'SabX2 Pratunum Wanton Mee', 'A must-go place every time visit Bangkok. Serves wanton mee I grew up with – simple, plain, and a lot of pork lard', 'Food & Drink', 3);

-- insert 10 dummy reviews
INSERT INTO `travelkaki`.`review` (`poi_id`, `comments`, `rating`, `user_id`) VALUES (1, 'Breathtaking. Come during sunset', 5, 1);
INSERT INTO `travelkaki`.`review` (`poi_id`, `comments`, `rating`, `user_id`) VALUES (1, 'Be careful of monkeys lurking around. They are quite fierce', 3, 2);
INSERT INTO `travelkaki`.`review` (`poi_id`, `comments`, `rating`, `user_id`) VALUES (1, 'What a beautiful place to watch the sunset. Hard to find info on weather', 4, 3);
INSERT INTO `travelkaki`.`review` (`poi_id`, `comments`, `rating`, `user_id`) VALUES (1, 'It was very beautiful place, but too much tourists.', 3, 4);
INSERT INTO `travelkaki`.`review` (`poi_id`, `comments`, `rating`, `user_id`) VALUES (1, 'Wonderful sunset but a bit overhyped and overpriced!', 3, 5);
INSERT INTO `travelkaki`.`review` (`poi_id`, `comments`, `rating`, `user_id`) VALUES (2, 'Great for kids but joins between sections are not well sealed, so they are quite uncomfortable for adults', 4, 6);
INSERT INTO `travelkaki`.`review` (`poi_id`, `comments`, `rating`, `user_id`) VALUES (2, 'The kids play area is pretty big, with lounge chairs right in front of it, so you can relax and keep an eye on the kids', 5, 7);
INSERT INTO `travelkaki`.`review` (`poi_id`, `comments`, `rating`, `user_id`) VALUES (2, 'They have free floating vests for young kids... we could not fault this place.', 5, 8);
INSERT INTO `travelkaki`.`review` (`poi_id`, `comments`, `rating`, `user_id`) VALUES (2, 'This place is perfect for young kids on a hot Balinese day', 4, 9);
INSERT INTO `travelkaki`.`review` (`poi_id`, `comments`, `rating`, `user_id`) VALUES (2, 'Was expensive, tickets and food, but kids had a ball!', 3, 10);

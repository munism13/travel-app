//Load libraries
const path = require("path");
const q = require("q");
const express = require("express");
var bodyParser = require("body-parser");

//Create an instance of express
const app = express();


app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

//Static routes
app.use(express.static(path.join(__dirname,'../client')));


//Configure the server
const port = process.env.APP_PORT || 3000;
app.listen(port, function() {
	console.info("Travel Guru started on port %d", port);
});



// Travel Kaki
// Server-side API calls

// =============== SUMMARY OF ENDPOINTS, GROUPED BY FUNCTIONS ===============
// User-related
// 1. Sign in a user                POST /login
// 2. Sign out a user               GET /logout
// 3. Register a user               POST /api/users
// 4. View my profile               GET /api/users/:userId
// 5. Update my profile             PUT /api/users/:userId
// Note: No Delete user function in the project scope

// POI-related
// 1. Display all POIs/search       GET /api/pois
// 2. Display a POI plus reviews    GET /api/pois/:poiId
// 3. Add a POI                     POST /api/pois
// 4. Update a POI                  PUT /api/pois/:poiId
// Note: No Delete POI function in the project scope

// Review-related
// 1. Add a review                  POST /api/reviews
// 2. Update a review               PUT /api/reviews/:reviewId
// 3. Delete a review               DELETE /api/reviews/:reviewId

// =============== SUMMARY OF ENDPOINTS, GROUPED BY ENDPOINTS ===============
// POST 	/login                  Sign in a user
// GET 	    /logout                 Sign out a user

// POST     /api/users              Register a user
// GET      /api/users/:userId      View my profile
// PUT      /api/users/:userId      Update my profile

// GET      /api/pois               Display all POIs/search
// GET      /api/pois/:poiId        Display a POI plus reviews
// POST     /api/pois               Add a POI
// PUT      /api/pois/:poiId        Update a POI

// POST     /api/reviews            Add a review
// PUT      /api/reviews/:reviewId  Update a review
// DELETE   /api/reviews/:reviewId  Delete a review

// =============== START OF SERVER CODE ===============
'use strict';

// basic server libraries
const express = require('express');
const path = require('path');

// to extract values sent via HTTP
const bodyParser = require('body-parser');

// database requirements & dependencies
// for sequelize, must npm install sequelize and mysql2
const sequelize = require('sequelize'); 

// authentication library requirements
const session = require('express-session');                 // for session management
const passport = require('passport');                       // authentication
const LocalStrategy = require('passport-local').Strategy;   // for local id and password account
const bcrypt = require('bcryptjs');                         // to 'encrypt' & 'decrypt' password

const app = express();

// =============== DATABASE-RELATED SETUP & FUNCTION CALLS ===============
// set up connection to Travel Kaki database
// INSTRUCTION: change MYSQL_USERNAME and MYSQL_PASSWORD to own database settings
const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'password';
var tkDb = new sequelize(
    'travelkaki',        // database name
    MYSQL_USERNAME, // login ID
    MYSQL_PASSWORD, // login password
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

// load tables for Travel Kaki operation: User, Poi and Review
var User = require('./models/user')(tkDb, sequelize);
var Poi = require('./models/poi')(tkDb, sequelize);
var Review = require('./models/review')(tkDb, sequelize);

// Every review is written by a User
Review.belongsTo(User, {foreignKey: 'user_id'});

// Every POI is created by a User
Poi.belongsTo(User, {foreignKey: 'createdby'});

// A POI has many reviews
Poi.hasMany(Review, { foreignKey: 'poi_id' });
// We can use a Review ID to look up the POI description
// Review.belongsTo(Poi, {foreignKey: 'poi_id'});

// =============== START: AUTHENTICATION-RELATED SETUP & FUNCTION CALLS ===============

// Section to implement authentication strategies and to manage lifecycle of those strategies
// For our Travel Kaki app, we'll start with passport-local
// Just using a supplied ID (we use email) and password

// Second port of call for passport-local
// To define how we will be authenticating the user locally
// hardcoded an authenticate user "ken@ken.com" into done(null, "ken@ken.com")
function authenticateUser(username, password, done) {
    // locate user in database based on supplied email address
    User.findOne({
        where: {
            email: username
        }
    })
        .then(function(user) {
            // if user does not exist in system, authentication is invalid 
            if (!user) {
                return done(null, false);
            } else {
                // determine if passwords match
                if (bcrypt.compareSync(password, user.dataValues.password)) {
                    // when passwords match, user is authenticated . Pass 'user' details on                 
                    return done(null, user);
                } else {
                    // if passwords don't match, authentication is invalid  
                    return done(null, false);
                }
            }
        }).catch(function(err) {
            // any other errors, authentication is invalid
            return done(err, false);
        });
};

// First port of call for passport-local, which expects only 2 fields: username and password
// map incoming 'email' to username, incoming 'password' to password
// calls authenticateUser() -- function declared above this -- for next step
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, authenticateUser));

// serializer saves user id to session (req.session.passport.user)
// NOTES from scotchmedia.com
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.
//   Typically, this will be as simple as storing the user ID when serializing,
//   and finding the user by ID when deserializing.
passport.serializeUser(function(user, done) {
    // passing user.id out for deserializer to search database by ID
    done(null, user.dataValues.id);
});

// deserializer follows by using the id being passed in to confirm user exists in db
passport.deserializeUser(function(userId, done) {
    User.findById(userId, {
        attributes: ['id', 'email', 'firstname', 'lastname', 'nickname']
    })
        .then(function(user) {
            if (user) {
                done(null, user);
            }
        }).catch(function(err) {
            done(null, false);
        });
});

// set up a session & inialise passport
app.use(session({
    secret:"welovetravelkaki",
    resave: false,
    saveUninitialized: true
}));

app.use(passport.initialize()); // passport now intercepts at express layer
app.use(passport.session());    // passport now integrates with newly-declared session

// =============== END: AUTHENTICATION-RELATED SETUP & FUNCTION CALLS ===============

// set up body-parser to read JSON data during PUT, with preventive size limit
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
app.use(bodyParser.json({limit: '10mb'}));

// =============== START: TRAVEL KAKI-SPECIFIC ENDPOINTS & API CALLS ===============

// Login a user
// INPUT: Expecting JSON object with just 'email' and 'password', which is automatically handled by passport
// {
//    email: <email value>,
//    password: <password value>
// }
// OUTPUT: Returns success/fail to front-end request, as well as user data
app.post("/login",
    function(req, res, next) {
        passport.authenticate('local',
            // passport Documentation
            //   If an exception occurred, 'err' will be set.
            //   If authentication failed, 'user' will be set to false
            //   Any details provided by strategy verify callback goes into 'info'
            function(err, user, info) {
                // if server encounters an exception or error
                if (err) {
                    return next(err);
                };
                // if authentication failed, either invalid email or password
                // return 401 - Unauthorized, and pass 'info' details
                if (!user) {
                    res.status(401).json({err: info});
                };
                // passport req.login() establishes a login session.
                // authenticate() invokes req.login() automatically
                // Primarily used when users sign up, during which req.login() can be invoked
                // to automatically log in newly-registered user.
                // upon establishing a successful session, 'user' values will be assigned to req.user
                req.login(user, function(err) {
                    if (err) {
                        // if server somehow cannot establish a session, return 500 - Internal Error
                        res.status(500).json({err: 'Could not log in user'});
                    } else {
                        res.status(200).json({user: req.user});
                    };
                });
            })(req, res, next);
});

// Logout a user
// INPUT: Expects nothing
// OUTPUT: Returns success/fail to front-end request
app.get("/logout", function (req, res) {
    req.logout();
    req.session.destroy();
    res.status(200);
    res.end();
});

// =============== START: USER-RELATED API CALLS ===============
// REGISTER a user
// INPUT: Expecting JSON object called 'user'
// {
//    "user": {
//          "email": "<email value>",
//          "password": "<password value>",
//          "confirmpassword": "<password value>",
//          "firstname": ...,
//          "lastname": ...,
//          "nickname": ...
// }
//
// At front-end app.js code, JSON object is declared like this in your controller or service:
//      return $http({
//          method: 'POST',
//          url: '/api/users/',
//          data: {user: newUser}
//      });
// where 'newUser' is from your angular controller object
//      e.g. addUserCtrl.newUser
// bound in HTML form using ng-model
//      e.g. ng-model="addUserCtrl.newUser.email"
//
// for POST and PUT endpoint calls, we use 'data:' to encapsulate the JSON object
// for GET endpoint calls, we use 'params:' -- explained later
//
// OUTPUT: Returns success/fail to front-end request
app.post("/api/users", function (req, res) {
    console.log("received user data: ", JSON.stringify(req.body));
    // verify that confirmation password matches original password keyed in
    // otherwise return an error
    // if(!(req.body.user.password === req.body.user.confirmpassword)) {
    //     return res.status(400).json({err: 'Passwords mismatch'});
    // }
    // encrypt (hash) the password before storing
    var hashPassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    
    // Use sequelize findOrCreate call which checks for email duplicates,
    // since database has been configured to accept unique email addresses
    // If none, create the user record
    User.findOrCreate({
        where: {
            email: req.body.email
        },
        defaults: {
            email: req.body.email,
            password: hashPassword,             // store the hashed-up password for security
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            nickname: req.body.nickname
        }})
            .spread(function(user, created) {
                // if user is successfully created
                if(created) {
                    res.status(201);                // From RESTful Web APIs: Created
                    res.type("application/json");
                    res.end();
                } else {
                    // if email already exists and user was not created, return an error
                    res.status(409);                // From Restful Web APIs: Conflict
                    res.type("application/json");
                    res.json(null);                 // Anything else we can return?
                };
            }).error(function(err){
                // else if a database error occurred instead
                res.status(500);                    // From Restful Web APIs: Server error
                res.type("application/json");
                res.json(err);
            });
});

// View my profile
// INPUT: Expecting 'userId' in endpoint
//      e.g. /api/users/23
// OUTPUT: Returns full profile of user (except password)
// User data: id, email, firstname, lastname, nickname
app.get("/api/users/:userId", function (req, res) {
    // get userId from endpoint and convert to integer to query database
    var userId = parseInt(req.params.userId);
    
    // Use sequelize findById call to quickly locate a user by primary ID field
    User.findById(userId, {
        attributes: ['id', 'email', 'firstname', 'lastname', 'nickname']
    })
        .then(function(result){
            console.log(result.dataValues);
            res.status(200);
            res.type("application/json");
            res.json(result.dataValues);
        }).catch(function(err){
            res.status(404);
            res.type("application/json");
            res.json(err);
        });
});

// Update my profile
// INPUT: Expecting 2 pieces of data
// 1. 'userId' in endpoint
//      e.g. /api/users/23
// 2. JSON object called 'user'
// {
//    "user": {
//          "firstname": ...,
//          "lastname": ...,
//          "nickname": ...
// }
// Note: Cannot change email since that is the user's primary username
//
// OUTPUT: Returns success/fail to front-end request
app.put("/api/users/:userId", function (req, res) {
    // get userId from endpoint and convert to integer to query database
    var userId = parseInt(req.params.userId);

    // Use sequelize update command to save edited user profile
    User.update(
        {
            firstname: req.body.user.firstname,
            lastname: req.body.user.lastname,
            nickname: req.body.user.nickname
        },{
            where: {id: userId}
        })
            .then(function(result){
                res.status(200);
                res.type("application/json");
                res.end();
            }).catch(function(err){
                res.status(500);
                res.type("application/json");
                res.json(err);
            });
});
// =============== END: USER-RELATED API CALLS ===============

// =============== START: POI-RELATED API CALLS ===============
// Display all POIs/search
// INPUT: Nothing expected at endpoint.
//        However, can pass in keyword search, for e.g. filter by City
//
// At front-end app.js code, JSON object is declared like this in your controller or service:
//      return $http({
//          method: 'GET',
//          url: '/api/pois/',
//          params: {
//              keyword: wordsToSearch,
//              city: cityFilter,
//              sortBy: fieldToSortBy
//              sortOrder: preferredSortOrder
//          }
//      });
// where 'wordsToSearch', 'cityFilter', fieldToSortBy', 'preferredSortOrder'
// come from your angular controller object
//      e.g. listPoiCtrl.wordsToSearch
// bound in HTML form using ng-model
//      e.g. ng-model="listPoiCtrl.wordsToSearch"
//
// NOTE: If you leave out any of the values in params, code will set defaults
//       which you can specify here on server-side
//
// for GET endpoint calls, we use 'params:' to string the query
//   e.g. localhost/api/pois?keyword=central&city=bangkok&sortBy=poiname&sortOrder=DESC
// for POST and PUT endpoint calls, we use 'data:' -- explained earlier
//
// OUTPUT: Returns preliminary/basic POI details
// POI data: id, city, poiname, description, category
// can pull in 'createdAt' for a date field use
app.get("/api/pois", function (req, res) {

    // manipulate incoming database query variables by setting defaults if no incoming values
    var keyword = req.query.keyword || "";          // set to blank to retrieve ALL POIs
    var filterCity = req.query.city || "Bali"       // set to Bali by default
    var sortBy = req.query.sortBy || "category";    // sort by category by default
    var sortOrder = req.query.sortOrder || "ASC";   // sort in ascending order; 'DESC' for descending

    Poi.findAll({
        // retrieve the POI fields we need
        attributes: ['id', 'city', 'poiname', 'description', 'category'],
        where: {$and: [
            // filter POIs by city
            { city: filterCity},
            // searches for keyword in POI name, description, category
            {$or: [
                {poiname: {$like: '%' + keyword + '%'}},
                {description: {$like: '%' + keyword + '%'}},
                {category: {$like: '%' + keyword + '%'}}
            ]}
        ]},
        order: [[sortBy, sortOrder]],
        limit: 20   // limit number of records to 20; you can change or remove
    })
        .then(function(results){
            res.status(200);
            res.type("application/json");
            res.json(results);
        }).catch(function(err){
            res.status(404);
            res.type("application/json");
            res.json(err);
        });
});

// Display a POI plus reviews
// INPUT: Expecting 'poiId' in endpoint
//      e.g. /api/pois/17
// OUTPUT: Returns full POI details, including user reviews
// POI data: id, city, poiname, description, category, createby
// Review data: id, comments, rating
// User data: nickname
app.get("/api/pois/:poiId", function (req, res) {
    console.log(">>>>",req.params)
    console.log(">>>>",req.body)
    // poiId needs to be converted to an integer to query the database with
    var poiId = parseInt(req.params.poiId);
    
    Poi.findById(poiId, {
        // retrieve the POI fields we need
        attributes: ['id', 'city', 'poiname', 'description', 'category', 'createby'],
        include: [{
            model: Review,
            // retrieve the Review fields we need
            attributes: ['id', 'comments', 'rating'],
            include: [{
                model: User,
                attributes: ['nickname']
            }],
        }],
        order: [
            [{model: Review}, 'createdAt', 'DESC']],        
        limit: 20
    }
)
        .then(function(result){
            res.status(200);
            res.type("application/json");
            res.json(result.dataValues);    
        }).catch(function(err){
            res.status(404);
            res.type("application/json");
            res.json(err);
        });
});

// Add a POI
// INPUT: Expecting JSON object called 'poi'
// {
//    "poi": {
//          "city": <city value>,
//          "poiname": <poiname value>,
//          "description": <description value>,
//          "category": ...,
//          "createby": ...
// }
//
// At front-end app.js code, JSON object is declared like this in your controller or service:
//      return $http({
//          method: 'POST',
//          url: '/api/pois/',
//          data: {poi: newPoi}
//      });
// where 'newPoi' is from your angular controller object
//      e.g. addPoiCtrl.newPoi
// bound in HTML form using ng-model
//      e.g. ng-model="addPoiCtrl.newPoi.poiname"
//
// for POST and PUT endpoint calls, we use 'data:' to encapsulate the JSON object
// for GET endpoint calls, we use 'params:' -- explained earlier
//
// OUTPUT: Returns success/fail to front-end request
app.post("/api/pois", function (req, res) {
    // Use Sequelize create call to just create new POI
    // That means that could be duplicate POIs but created under different IDs
    Poi.create({
        city: req.body.city,
        poiname: req.body.poiname,
        description: req.body.description,
        category: req.body.category,
        createby: parseInt(req.body.createby) || 2 // store creator's ID which is a number
    }).then(function(result) {
        res.status(200);
        res.type("application/json");
        res.end();
    });
});

// Update a POI
// INPUT: Expecting 2 pieces of data
// 1. 'poiId' in endpoint
//      e.g. /api/pois/17
// 2. JSON object called 'poi'
// {
//    "poi": {
//          "city": <city value>,
//          "poiname": <poiname value>,
//          "description": <description value>,
//          "category": ...,
//          "createby": ...
// }
// INSTRUCTION: Can anyone change the values of a POI?
// Or changes permitted only by same creator? Can check using 'createby' field
//
// OUTPUT: Returns success/fail to front-end request
app.put("/api/pois/:poiId", function (req, res) {
    // poiId needs to be converted to an integer to query the database with
    var poiId = parseInt(req.params.poiId);
    
    // Use sequelize update to locate the right poiId and write changes back to the database
    Poi.update(
        {
            city: req.body.poi.city,
            poiname: req.body.poi.poiname,
            description: req.body.poi.description,
            category: req.body.poi.category,
            createby: parseInt(req.body.poi.createby) // store creator's ID which is a number
        },{
            where: {id: poiId}
        })
            .then(function(result){
                res.status(200);
                res.type("application/json");
                res.end();
            }).catch(function(err){
                res.status(500);
                res.type("application/json");
                res.json(err);
            });
});

// Placeholder server code, even though this is NOT in project scope
// INSTRUCTION: Decide whether you need to enable deletion of POI
// as there is dependency to delete ALL reviews linked to it too
//
// Delete a POI
// INPUT: Expecting 'poiId' in endpoint
//      e.g. /api/pois/17
// OUTPUT: Returns success/fail to front-end request
app.delete("/api/pois/:poiId", function (req, res) {
    var poiId = parseInt(req.params.poiId);

    Poi.destroy({
        // soft delete; check deleteAt column for timestamp
        where: {id: poiId}
    })
        .then(function(result){
            res.status(200);
            res.type("application/json");
            res.end();
        }).catch(function(err){
            res.status(404);
            res.type("application/json");
            res.json(err);
        });
});
// =============== END: POI-RELATED API CALLS ===============

// =============== START: REVIEW-RELATED API CALLS ===============

// Add a review
// INPUT: Expecting JSON object called 'review'
// {
//    "review": {
//          "comments": <comments value>,
//          "rating": <rating value>,
//          "poi_id": <poi_id value>,
//          "user_id": ...
// }
//
// At front-end app.js code, JSON object is declared like this in your controller or service:
//      return $http({
//          method: 'POST',
//          url: '/api/reviews/',
//          data: {review: newReview}
//      });
// where 'newReview' is from your angular controller object
//      e.g. addReviewCtrl.newReview
// bound in HTML form using ng-model
//      e.g. ng-model="addReviewCtrl.newReview.comments"
//
// for POST and PUT endpoint calls, we use 'data:' to encapsulate the JSON object
// for GET endpoint calls, we use 'params:' -- explained earlier
//
// OUTPUT: Returns success/fail to front-end request
app.post("/api/reviews", function (req, res) {
    // Use Sequelize create call to just create new review
    // That means that could be duplicate POIs but created under different IDs
    Review.create({
        comments: req.body.comments,
        rating: req.body.rating || 5,
        poi_id: parseInt(req.body.poi_id) || 2,
        user_id: parseInt(req.body.user_id) || 2
    }).then(function(result) {
        res.status(200);
        res.type("application/json");
        res.end();
    });
});

// Update a review
// INPUT: Expecting 2 pieces of data
// 1. 'reviewId' in endpoint
//      e.g. /api/reviews/5
// 2. JSON object called 'review'
// {
//    "review": {
//          "comments": <comments value>,
//          "rating": <rating value>,
// }
// Note: Assume review is for same POI by same user, so poi_id and user_id are not changed
//
// OUTPUT: Returns success/fail to front-end request
app.put("/api/reviews/:reviewId", function (req, res) {
    // reviewId needs to be converted to an integer to query the database with
    var reviewId = parseInt(req.params.reviewId);
    
    // Use sequelize update to locate the right reviewId and write changes back to the database
    Review.update(
        {
            comments: req.body.review.comments,
            rating: req.body.review.rating,    
        },{
            where: {id: reviewId}
        })
            .then(function(result){
                res.status(200);
                res.type("application/json");
                res.end();
            }).catch(function(err){
                res.status(500);
                res.type("application/json");
                res.json(err);
            });
});

// Delete a review
// INPUT: Expecting 'reviewId' in endpoint
//      e.g. /api/reviews/5
// OUTPUT: Returns success/fail to front-end request
app.delete("/api/reviews/:reviewId", function (req, res) {
    var reviewId = parseInt(req.params.reviewId);

    Review.destroy({
        // soft delete; check deleteAt column for timestamp
        where: {id: reviewId}
    })
        .then(function(result){
            res.status(200);
            res.type("application/json");
            res.end();
        }).catch(function(err){
            res.status(404);
            res.type("application/json");
            res.json(err);
        });
});
// =============== END: TRAVEL KAKI-SPECIFIC ENDPOINTS & API CALLS ===============

// =============== OTHER SERVER SETUP CALLS ===============

// define paths to required static files 
app.use(express.static(path.join(__dirname,'../client')));

// error message when user visits an non-existent page
app.use(function(req, res, next) {
    res.send("<h2>TravelGuru concierge: Sorry, no such destination.</h2>");
    next();
});

// set up port and start server
const APP_PORT = process.env.PORT || 3000;
app.listen(APP_PORT, function() {
    console.log('TravelGuru app started on port: ', APP_PORT);
});
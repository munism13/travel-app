// model definition of 'poi' table in 'travelkaki' database
module.exports = function(database, Sequelize) {
    // 'poi' is the same name as the SQL table
    var Poi = database.define('poi', {
        // map everything here just like it is on table 'poi'
            id: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            city: {
                type: Sequelize.STRING(45),
                allowNull: false,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            country: {
                type: Sequelize.STRING(45),
                allowNull: true,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            poiname: {
                type: Sequelize.STRING(45),
                allowNull: false,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            description: {
                type: Sequelize.STRING(255),
                allowNull: true,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            category: {
                type: Sequelize.STRING(30),
                allowNull: false,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            imagelink: {
                type: Sequelize.STRING(255),
                allowNull: true,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            createby: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                references: {
                    model: 'User',
                    key: 'id'
                }
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: true
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: true
            },
            deletedAt: {
                type: Sequelize.DATE,
                allowNull: true
            }
        }, {
            // 'poi' is the table name on the 'travelkaki' database
            tableName: 'poi',
            // Allow timestamp attributes (createdAt, updatedAt, deletedAt)
            // By default, added to know when db entry added & last updated
            timestamps: true,
            paranoid: true
        }
    );
    return Poi;
};
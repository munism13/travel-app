// model definition of 'review' table in 'travelkaki' database
module.exports = function(database, Sequelize) {
    // 'review' is the same name as the SQL table
    var Review = database.define('review', {
        // map everything here just like it is on table 'review'
            id: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            comments: {
                type: Sequelize.STRING(255),
                allowNull: true,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            rating: {
                type: Sequelize.INTEGER(1),
                allowNull: false
            },
            poi_id: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                references: {
                    model: 'Poi',
                    key: 'id'
                }
            },
            user_id: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                references: {
                    model: 'User',
                    key: 'id'
                }
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: true
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: true
            },
            deletedAt: {
                type: Sequelize.DATE,
                allowNull: true
            }
        }, {
            freezeTableName: true,
            // 'review' is the table name on the 'travelkaki' database
            tableName: 'review',
            // Allow timestamp attributes (createdAt, updatedAt, deletedAt)
            // By default, added to know when db entry added & last updated
            timestamps: true,
            paranoid: true
        }
    );
    return Review;
};
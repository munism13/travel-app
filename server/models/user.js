// model definition of 'user' table in 'travelkaki' database
module.exports = function(database, Sequelize) {
    // 'user' is the same name as the SQL table
    var User = database.define('user', {
        // map everything here just like it is on table 'user'
            id: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            email: {
                type: Sequelize.STRING(45),
                allowNull: false,
                unique: true,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            password: {
                type: Sequelize.STRING(64),
                allowNull: false,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            firstname: {
                type: Sequelize.STRING(45),
                allowNull: false,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            lastname: {
                type: Sequelize.STRING(45),
                allowNull: false,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            nickname: {
                type: Sequelize.STRING(45),
                allowNull: true,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            nationality: {
                type: Sequelize.STRING(45),
                allowNull: true,
                charset: 'utf8',
                collate: 'utf8_unicode_ci'
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: true
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: true
            },
            deletedAt: {
                type: Sequelize.DATE,
                allowNull: true
            }
        }, {
            freezeTableName: true,
            // 'user' is the table name on the 'travelkaki' database
            tableName: 'user',
            // Allow timestamp attributes (createdAt, updatedAt, deletedAt)
            // By default, added to know when db entry added & last updated
            timestamps: true,
            paranoid: true

        }
    );
    return User;
};